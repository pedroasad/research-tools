#!/usr/bin/python
# 
# The MIT License (MIT)
# 
# Copyright (c) 2015 Pedro Asad
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""
Usage: citation-graph.py <.bib file> [pdf directory]

Produces a Graphviz plot depicting citation relationships among a collection of 
papers. The BibTeX file must contain an additional `file` field for every entry
naming the file of the corresponding paper. Every paper is converted to plain 
text using the pdf2txt utility and the title of the other papers in the .bib 
database are sought to confirm citations. Character case and punctuation are
ignored for purposes of file name and title matching.

If [pdf directory] is empty, the current directory is assumed.

Depends on the bibtexparser python module and on PDF Lab's pd2txt utility.
"""

from bibtexparser import dumps as bibdump, loads as bibparse
from os import popen, system as shell, listdir
from pdb import set_trace
from re import match
from sys import argv, exit
from textwrap import wrap

# TODO: Write documentation
# TODO: Sort nodes by year (decreasingly) and main author's surname

pdffiles = {}

def printHelp():
	print '''\
Usage:
	%s <.bib file> <pdf directory>''' % (__file__)

def readpdf(pdftitle):
	fname = pdffiles[pdftitle]
	return unicode(squeezeText(popen('pdf2txt "%s"' % fname).read()), 'utf8')

def squeezeText(title):
	return title.replace('\n', '').replace('-', '').replace(' ', '').lower()

def parseCitationKey(citationKey):
	return match(r'(\D+)(\d+)\D+', citationkey).groups()

if __name__ == '__main__':
	if len(argv) < 2:
		printHelp()
		exit(1)

	if len(argv) == 3:
		cwd = argv[2]
	else:
		cwd = '.'

	try:
		bibfile = open(argv[1])
	except IOError as err:
		print err
		exit(1)

	pdffiles = { squeezeText(fname) : cwd + '/' + fname for fname in listdir(cwd) if fname[-3:] == 'pdf' }
	database = bibparse(bibfile.read())
	entriesbytitle = { squeezeText(entry['title']) : entry for entry in database.entries }

	for entry in database.entries:
		citations = []

		if 'file' not in entry:
			print 'Warning: no file for entry"%s"' % entry['title']
		elif squeezeText(entry['title']) != squeezeText(entry['file']).replace('.pdf', ''):
			print 'Warning:\n\tunusual file name "%s"\n\tfor title "%s"' % (entry['file'], entry['title'])
		else:
			currtitle = squeezeText(entry['title'])
			currtext  = readpdf(squeezeText(entry['file']))
			for t, e in entriesbytitle.items():
				if t != currtitle and int(entry['year']) >= int(e['year']) and t in currtext:
					citations.append(e['ID'])

		entry['citations'] = ','.join(citations)

	print 'digraph Citations {'
	for entry in database.entries:
		print '''
	%s [
		label = "%s"
	];
''' % (entry['ID'], '\\n'.join(wrap(entry['title'], 20)))

	for entry in database.entries:
		refs = entry['citations'].split(',')
		if len(refs) > 0:
			edges = ['\t%s -> %s;' % (entry['ID'], ref) for ref in refs]
			print '\n'.join(edges)
	print '}'
