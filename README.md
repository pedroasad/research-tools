# Research tools
<span style="text-align:center; font-style:italic;">
	A collection of tools to aid in research activities
</span>

## Introduction

Academic research routine can be a pain in the ass: browsing papers in Google
Scholar; downloading tons of PDFs — hum, where do I put these? — keeping BibTeX
databases up-to-date; keeping notes about certain papers; categorizing and
grouping of papers; reading back previously fetched papers and sipping through
your annotations — where, on Earth, are my annotations? — telling the relevant
from the junk; and, possibly the worst of all, figuring out relations among a 
handful of papers so you can understand the chronological development of an area
and identify the most promising spots where the knowledge frontier is waiting 
to be gently pushed. 

I guess you got the whole point already. Making sense out of the pile of works
available on research portals is like finding a needle in a haystack, barely
impossible if you don't have a magnet, or in this case, _feeling_. Experienced
researchers have this feeling, as they have been doing this for some time, but
what if you're a young researcher whose advisor is rather busy to have some time
for you, say once every semester? And even for experienced researchers, these
tasks may be extremely boring and inneficient, given the existing toolset.

Research assisting tools fill some of the gaps sparsely, but are scattered out
there. Some are paid, others free to use, but with limited features unless you
buy the full version, e.g. [Mendeley](https://www.mendeley.com),
[Zotero](https://www.zotero.org) and so on. There are even some nice open-source
alternatives, like [I-librarian](https://i-librarian.net/), and more focused
applications, like [JabRef](jabref.sourceforge.net), in the realm of reference
management. But in the end, there is no Swiss Army Knife and all existing
software focuses on some aspect of research routine, but none really seeks to be
an inteligent and self-contained ``research assistant``.

So, what's the deal? What can Research Tools offer? Right now, besides the
complaints, and the noble purpose of creating the perfect research assitance
tool suite in the not so far future, it comes with a prototype tool to look at a
set of PDFs and a BibTeX database and produce a [Graphviz](www.graphviz.org)
plot of related papers based on citations among them.

---
## Tools

### `citation-graph.py` — get a citation graph from a `.bib` and some `.pdf`'s

Provided you have a directory with properly named PDF files and a `.bib` file
with information on the papers of said files, it will output a `.dot` containing
a citation graph. The down side right now is that you must add a `file` field in
the `.bib` file with the name of the corresponding `.pdf` to every entry. This
is because references are sought through brute force searching the other paper's 
titles in the citing paper's plain text. It is not efficient, I know!

It depends on [PDF Lab's](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit)
`pdf2txt` to convert the `.pdf`'s to plain text and on the
[bibtexparser](https://pypi.python.org/pypi/bibtexparser/0.6.1) python module to
parse BibTeX data.

---
## Future directions

### Parsing citations from PDFs

There is an interesting service provided by [CrossRef
Labs](labs.crossref.org/pdfextract) that allows extracting references from PDF
files. I could not get it to run in my first few attempts, due to some strange
Ruby packaging dependency error I could not fix — no ruby expertiese here! But
using their service via HTTP or getting it to run would allow to obtain the
citations and a much more efficient (perhaps even reliable) than the current
`pdf2txt` strategy.

This tool may even help to sort the works based on how many times they are cited
from a single paper. Thus, we can tell strong from weak relations among papers
and produce graphical output that reflects that difference.

### Automatic metadata extraction

Instead of copying BibTeX data and pasting it into your `.bib` database, just
throw PDFs into a folder and let this feature do the job for you. Most softwares
cited in the introduction section of this document can do something like this.

### Automatic renaming of files

Right now, you need to rename your PDF files to match an additional `file` field
in the `.bib` file than you must also create. It doesn't need to be the paper's
title, but you still must input this information twice.

---
## Contributing

If you think the project goals are noble and would like to contribute with the
current tools or add some functionality of yours, please don't hesitate to fork
us and do so!

There is no strict roadmap to follow, so if you're eager to further discuss and
develop the idea of a full-featured ``research assistant``, get in touch throgh
my e-mail address below. Then it might someday be rightful to change this
project's name from ``Research Tools`` to ``Research Assitant``.

### Maintainers

* Pedro Asad
	* [Gitlab profile](https://gitlab.com/u/psa-exe)
	* [E-mail](pasad@cos.ufrj.br)

---
## Licensing
This work is open-source and distributed under the MIT license. Check the
[LICENSE.txt](LICENSE.txt) file for further information.
